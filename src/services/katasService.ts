import { HeadsetSharp } from "@mui/icons-material";
import { AxiosRequestConfig } from "axios";
import axios from "../utils/config/axios.config";



export const getAllKatas = (token: string, limit?: number, page?: number) => {

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            limit: limit,
            page: page
        }
    }
    return axios.get('/katas', options)


}

export const getKataByID = (token: string, id: string) => {

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id: id
        }
    }
    console.log(token)
    return axios.get('/katas', options)


}

export const voteKata = (token: string, id: string, vote: number) => {

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id,
            vote: vote
        }

    }
    // Declare Body to post
    const body = { '': '' }
    return axios.put('/katas/vote', body, options)


}

export const solveKata = (token: string, id: string, solution: string) => {

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id,
        }

    }
    // Declare Body to post
    const body = { solution: solution }
    return axios.put('/katas/solve', body, options)


}