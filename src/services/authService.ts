import axios from '../utils/config/axios.config';

/**
 * Login user
 * @param {string} email 
 * @param {string} password 
 * @returns 
 */

export const login = (email: string, password: string) => {

    // Declare Body to post
    const body = {
        email: email,
        password: password
    }

    // Send POST request
    return axios.post('/auth/login', body)
}

/**
 * Register user
 * @param {string} name 
 * @param {string} email 
 * @param {string} password 
 * @param {number} age 
 * @returns 
 */
export const register = (name: string, email: string, password: string, age: number | undefined) => {

    // Declare Body to post
    const body = {
        name: name,
        email: email,
        password: password,
        age: age
    }

    // Send PUT request
    return axios.post('/auth/register', body)
}
