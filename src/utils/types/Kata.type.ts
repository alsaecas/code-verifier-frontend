export enum KataLevel {
    BASIC = 'Basic',
    MEDIUM = 'Medium',
    HIGH = 'High'
}
export type IVUser = {
    user: string,
    stars: number
}

export type Kata = {
    _id: number,
    name: string,
    description: string,
    level: KataLevel,
    tries: number,
    stars: {
        average: number,
        users: IVUser[]
    },
    creator: string, // ID of User creator
    solution: string,
    participants: string[]
}