// Import React FilePond
import { FilePond, registerPlugin } from 'react-filepond'

// Import FilePond styles
import 'filepond/dist/filepond.min.css'

// Import the Image EXIF Orientation and Image Preview plugins
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation'
import FilePondPluginImagePreview from 'filepond-plugin-image-preview'
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css'


import { useState } from 'react'
import { FilePondFile } from 'filepond'

// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview)

export const FilePondUploader = () => {
    const [files, setFiles] = useState<any>([])
  return (
      <FilePond
        files={files}
        onupdatefiles={setFiles}
        allowMultiple={true}
        maxFiles={3}
        server="http://localhost:8000/api/katas/upload"
        name="Your files" 
        labelIdle='Drag & Drop your files or <span class="filepond--label-action">Browse</span>'
      />
  )

}