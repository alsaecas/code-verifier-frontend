import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import { useNavigate } from 'react-router-dom';
import { login } from "../../services/authService";
import { AxiosResponse } from "axios";
import { useFormik } from "formik";
import * as Yup from 'yup';
import { Alert } from '@mui/material';
import { useState } from 'react';

// Define Schema of validation with Yup
const loginSchema = Yup.object().shape(
  {
    email: Yup.string().email('Invalid Email Format').required('Email is required'),
    password: Yup.string().required('Password is required')
  }
)

const theme = createTheme();

export const LoginMaterial = () => {
  const [alarm, setAlarm] = useState(false);
  let navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      login(values.email, values.password).then((response: AxiosResponse) => {
        if (response.status === 200) {
          if (response.data.token) {
            sessionStorage.setItem('sessionJWTToken', response.data.token);
            sessionStorage.setItem('sessionUserID', response.data.userID);
            navigate('/')
          } else {
            setAlarm(true);
            throw new Error('Error generating Login Token')
          }
        } else {
          setAlarm(true);
          throw new Error('Invalid credentials')
        }
      }).catch((error) => {
        setAlarm(true);
        console.error(`[LOGIN ERROR]: Something went wrong: ${error}`)
      }
      )
    },
  });


  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        { alarm &&
        <Alert
        
        severity="error"
        action={
          <Button color="inherit" size="small" onClick={() => {navigate('/register')}}>
            REGISTER?
          </Button>
        }
      >
        Incorrect Name or Password!
      </Alert>
                }
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={formik.handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="./register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}