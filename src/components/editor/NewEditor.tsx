import React, { Fragment, useState } from 'react';
import Editor from 'react-simple-code-editor';
import Highlight, { defaultProps, Language } from 'prism-react-renderer';
import theme from 'prism-react-renderer/themes/nightOwl';
import { Button } from '@mui/material';
import { solveKata } from '../../services/katasService';
import { useSessionStorage } from '../../hooks/useSessionStorage';
import { AxiosResponse } from "axios";
import { NavigateNextTwoTone } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';

const codeSnippet =
    `
//WRITE YOUR SOLUTION HERE


`
// DEfine Styles for Editor
const styles: any = {
    root: {
        boxSizing: 'border-box',
        fontFamily: '"Dark Mono", "Fira Code", monospace',
        ...theme.plain
    }
}

const languages: Language[] = [
    "tsx",
    "typescript",
    "javascript",
    "jsx",
    "python",
    "json",
    "go"
]
// Highligh Component
const HighlightElement = (code: string) => (
    <Highlight {...defaultProps} theme={theme} code={code} language={languages[0]}>
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
            <Fragment>
                { tokens.map((line, i) => (
                    <div {...getLineProps({ line: line, key: i})} >
                        { line.map((token, key) => <span {...getTokenProps({token, key})} /> )}
                    </div>
                )) }
            </Fragment>
        )}
    </Highlight>
);


export const NewEditor = (props: any) => {
    let navigate = useNavigate();
    const loggedIn = useSessionStorage('sessionJWTToken');
    const [code, setCode] = useState(codeSnippet);
    const [languageSelected, setLanguageSelected] = useState(languages[0]);

    const handleLanguageChange = (newValue: any) => {
        setLanguageSelected(newValue);
    }

    const handleCodeChange = (newCode: string) => {
        setCode(newCode);
    }
    const handleSolve = () => {
        if (props.id){
        solveKata(loggedIn,props.id,code).then((response: AxiosResponse) => {
        window.location.reload();
        
        }).catch((error) => console.error(`[SOLVE KATA ERROR]: ${error}`))
    }
      }
    return (
        <div>
            <select>
                {languages.map((language, index) => (
                    <option onChange={(value) => handleLanguageChange(value)} value={language} key={index}>{language}</option>
                ))}
            </select>

            <Editor
                value={code}
                onValueChange={handleCodeChange}
                highlight={HighlightElement}
                padding={10}
                style={styles.root}
            />
            <Button variant="contained" onClick={handleSolve}>Solve It!</Button>
        </div>
    )
}