import React, { ReactChild, useState } from "react";
import { useNavigate } from 'react-router-dom';

// Theme personalitzation for material UI
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";

//CSS & Drawer
import CssBaseline from "@mui/material/CssBaseline";
import MuiDrawer from "@mui/material/Drawer";

// Navbar
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

// Material lists
import List from "@mui/material/List";

// Material Grids and Boxs
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";

// Icons
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import LogoutIcon from "@mui/icons-material/Logout";
import NotificationsIcon from "@mui/icons-material/Notifications";

// Listo for the menu
import { MenuItems } from "./MenuItems";


// Width for Drawer Menu
const drawerWidth: number = 240;

// Props from AppBar
interface AppBarProps extends MuiAppBarProps {
    open?: boolean
}

// AppBar
const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => (
    {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        ...(open && {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            }),
        }),
    }
))

// Drawer Menu
const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        '& .MuiDrawer-paper': {
            position: 'relative',
            whiteSpace: 'nowrap',
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            }),
            boxSizing: 'border-box',
            ...(!open && {
                overflowX: 'hidden',
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen
                }),
                with: theme.spacing(7),
                // Breakpoint for Media Quieries
                [theme.breakpoints.up('sm')]: {
                    width: theme.spacing(9)
                }
            })
        }
    })
)


// Define theme
const myTheme = createTheme();

// Dashboard content
export const Dashboard = (props: any) => {
    let navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const toggleDrawer = () => {
        setOpen(!open);
    }
    const logoutClick = () => {
        sessionStorage.removeItem('sessionJWTToken');
        sessionStorage.removeItem('sessionUserID');
        navigate('/login')
    }
return (
    <ThemeProvider theme={myTheme}>
        <Box sx={{display:'flex'}}>
            <CssBaseline />
                {/* AppBar */}
                <AppBar position='absolute' open={open}>
                     {/* ToolBar --> Actions */}
                     <Toolbar sx={{pr: '24px'}}>
                          {/* ICON TO TOGGLE DRAWER MENU */}
                         <IconButton
                            edge='start'
                            color='inherit'
                            aria-label='open drawer'
                            onClick={toggleDrawer}
                            sx={{
                                marginRight: '36px',
                                ...(open && {
                                    display: 'none'
                                })
                            }}
                         >
                             <MenuIcon/>
                         </IconButton>
                          {/* Title of APP */}
                         <Typography
                            component='h1'
                            variant='h6'
                            color='inherit'
                            noWrap
                            sx={{
                                flexGrow: 1
                            }}
                         >
                            Code Verification Katas
                         </Typography>
                         {/* ICON to show notifications */}
                         <IconButton color='inherit'>
                             <Badge badgeContent={10} color='secondary'>
                                 <NotificationsIcon/>
                             </Badge>
                         </IconButton>
                         {/* ICON to Logout */}
                         <IconButton color='inherit' onClick={logoutClick}>                             
                                 <LogoutIcon/>                           
                         </IconButton>
                     </Toolbar>
                </AppBar>
                <Drawer variant='permanent' open={open}>
                    <Toolbar
                        sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            px: [1]
                        }}
                    >
                           {/* ICON to HIDE Menu */}
                           <IconButton color='inherit' onClick={toggleDrawer}>                             
                                 <ChevronLeftIcon/>                           
                         </IconButton>
                    </Toolbar>
                    <Divider/>
                    {/* LIST of Menu */}
                    <List component='nav'>
                        { <MenuItems/> }
                    </List>
                </Drawer>
                {/* Dashboard content */}
                <Box
                component='main'
                sx={{
                    backgroundColor: (theme) => theme.palette.mode==='light' ? theme.palette.grey[100] : theme.palette.grey[900],
                    flexGrow: 1,
                    heigh: '100vh',
                    overflow: 'auto'
                }}
                >
                     {/* Toolbar */}
                     <Toolbar/>
                       {/* Container with content */}
                       {/* TODO Change for the Navigation content by URL */}
                       <Container maxWidth='lg' sx={{ mt: 4, mg: 4}}>
                           <Grid item xs={12} md={12} lg={12}>
                               <Paper sx={{
                                   p: 2,
                                   display: 'flex',
                                   flexDirection: 'column',
                                   heigh: 400
                               }}>
                                  {<props.data/>}
                               </Paper>
                           </Grid>
                       </Container>
                </Box>
        </Box>
    </ThemeProvider>
    )
}
