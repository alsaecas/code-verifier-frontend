
import React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

// Material Icon components
import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import { useNavigate } from 'react-router-dom';



export const MenuItems  = () =>{
    let navigate = useNavigate();
    return(
    <React.Fragment>
        {/* Dashboard to Katas Button */}
        <ListItemButton onClick={() => navigate('/katas')}>
            <ListItemIcon>
                <DashboardIcon/>
            </ListItemIcon>
            <ListItemText primary="Katas"/>
        </ListItemButton>
        {/* Users Button */}
        <ListItemButton>
            <ListItemIcon>
                <PeopleIcon/>
            </ListItemIcon>
            <ListItemText primary="Users"/>
        </ListItemButton>
        {/* Ranking */}
        <ListItemButton>
            <ListItemIcon>
                <BarChartIcon/>
            </ListItemIcon>
            <ListItemText primary="Ranking"/>
        </ListItemButton>

    </React.Fragment>
    )
}