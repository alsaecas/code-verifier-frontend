import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Copyright } from "./CopyRight";


export const StickyFooter = () => {
    return(
        <Box sx={
            {
            display: 'flex',
            flexDirection: 'column'
            }
        }>
            <CssBaseline/>
                <Box
                    component="footer"
                    sx={
                        {
                            py: 1,
                            px: 1,
                            mt: 'auto',
                            backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[200] : theme.palette.grey[800]
                        }
                    }
                >
                    <Container maxWidth="sm">
                        <Copyright sx={
                        {
                            pt:1
                        }
                        }/>
                    </Container>
                </Box>
        </Box>
    )
}