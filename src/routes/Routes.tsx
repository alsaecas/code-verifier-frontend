import { BrowserRouter as Router, Routes, Route, Navigate, Link } from 'react-router-dom'
import { KatasDetailPage } from '../pages/KatasDetailPage';
import { KatasPage } from '../pages/KatasPages';
import { RegisterPage } from '../pages/RegisterPage';
import { LoginPage } from '../pages/LoginPage';
import { HomePage } from '../pages/HomePage';
import { KatasCreatePage } from '../pages/KatasCreatePage';

export const AppRoutes = () => {
    return (
        <Routes>
          <Route path='/' element={<HomePage />}></Route>
          <Route path='/login' element={<LoginPage />}></Route>
          <Route path='/register' element={<RegisterPage />}></Route>
          <Route path='/katas' element={<KatasPage />}></Route>
          <Route path='/katas/:id' element={<KatasDetailPage />}></Route>
          <Route path='/katas/create' element={<KatasCreatePage />}></Route>
          {/* Redirect if no page */ }
          <Route 
            path='*' 
            element={<Navigate to='/' replace />}>
          </Route>
        </Routes>
    )
}